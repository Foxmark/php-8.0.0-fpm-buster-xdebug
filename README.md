# How to setup phpStorm to run phpUnit tests using Docker image

To download my PHP Xdebug image run this command in your bash terminal: `docker pull foxmark/php-8.0.0-fpm-buster-xdebug`

## Part 1
 - Go to `File` > `Settings` > `PHP` > `...` to show list of `CLI Interpreters`.

![Screen 1](https://drive.google.com/uc?id=1sC4gTYremf5hhLVva8kUy9F-QHTd82Cw "Screen 1")

 - Click `+` to create a new Interpreter.

 - Click `From Docker, Vagrant, VM, WSL, Remote...` option.
   
   
![Screen 2](https://drive.google.com/uc?id=19C38cwZVeR_qyV3_M8TTm0Kw9thj3FCe "Screen 2")
 
 - Select `Docker` and pick php image with xdebug extension enabled
  
![Screen 3](https://drive.google.com/uc?id=1IFRRPigMLAMXeqvZS5s1CSM0ZMiUpeGl "Screen 3")

 - Click `OK` to close the window.
 
 - Click `Apply` and close the `Settings` window.

## Part 2
 - From the main menu select `Add Configuration...`
   
![Screen 4](https://drive.google.com/uc?id=1rrIETajM_-Y7SqqO-Qub0Rqbtadlacsv "Screen 4")

 - Click `+` to create a new configuration and select `PHPUnit`

![Screen 5](https://drive.google.com/uc?id=1WxMELspLwrAca7_zzfdCOvb4bS5I6nUi "Screen 5")

 - Enter the config details:

 _Name: `phpunit.xml` (in my case)_

 _Select `Use alternative configuration file:` and enter the path to your phpunit.xml file_

 _Select `Interpreter` you created in **Part 1**_

 _Add Environment variables_ `XDEBUG_MODE=coverage;TZ=Europe/London`

 _Variable `TZ=Europe/London` is optional_

![Screen 6](https://drive.google.com/uc?id=1YyvLs-TUZvBVlSuZQ7_eEQubiYxgkKcM "Screen 6")

 - Click `Apply` to close the window.

 - Now you can run your new configuration

![Screen 7](https://drive.google.com/uc?id=1qpURiZvK8W2IAW-I1clKkqgSpCwhHtgY "Screen 7")